EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:switches
LIBS:relays
LIBS:motors
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:LCD-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L RC1602A U1
U 1 1 5B23F1E9
P 5050 3300
F 0 "U1" H 4800 3950 50  0000 C CNN
F 1 "RC1602A" H 5160 3950 50  0000 L CNN
F 2 "Displays:RC1602A" H 5150 2500 50  0001 C CNN
F 3 "" H 5150 3200 50  0001 C CNN
	1    5050 3300
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4200 3300 4350 3300
Wire Wire Line
	4200 2200 4200 3300
Text GLabel 4200 2200 1    60   Input ~ 0
5V
Wire Wire Line
	4850 2750 4850 2900
Text GLabel 4850 2750 1    60   Input ~ 0
POT
$Comp
L R R1
U 1 1 5B23F52C
P 4850 2400
F 0 "R1" V 4930 2400 50  0000 C CNN
F 1 "200" V 4850 2400 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 4780 2400 50  0001 C CNN
F 3 "" H 4850 2400 50  0001 C CNN
	1    4850 2400
	0    1    1    0   
$EndComp
Wire Wire Line
	5000 2400 5350 2400
Wire Wire Line
	4700 2400 4200 2400
Wire Wire Line
	5750 3300 5900 3300
Wire Wire Line
	5900 2750 5900 3550
Text GLabel 5900 3550 3    60   Input ~ 0
GND
Wire Wire Line
	5250 2900 5250 2750
Wire Wire Line
	5250 2750 5900 2750
Connection ~ 5900 3300
Wire Wire Line
	4650 3700 4650 3750
Wire Wire Line
	4650 3750 5150 3750
Wire Wire Line
	4850 3750 4850 3700
Wire Wire Line
	4950 3700 4950 3850
Connection ~ 4850 3750
Wire Wire Line
	5050 3750 5050 3700
Connection ~ 4950 3750
Wire Wire Line
	5150 3750 5150 3700
Connection ~ 5050 3750
Wire Wire Line
	4550 3700 4550 3850
Wire Wire Line
	4750 3700 4750 3850
Wire Wire Line
	5250 3700 5250 3850
Wire Wire Line
	5350 3700 5350 3850
Wire Wire Line
	5450 3700 5450 3850
Wire Wire Line
	5550 3700 5550 3850
Connection ~ 4200 2400
$Comp
L Conn_01x09 J1
U 1 1 5B23F7BA
P 6650 3200
F 0 "J1" H 6650 3700 50  0000 C CNN
F 1 "Conn_01x09" H 6650 2700 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x09_Pitch2.54mm" H 6650 3200 50  0001 C CNN
F 3 "" H 6650 3200 50  0001 C CNN
	1    6650 3200
	1    0    0    -1  
$EndComp
Text GLabel 4550 3850 3    60   Input ~ 0
RS
Text GLabel 4750 3850 3    60   Input ~ 0
E
Text GLabel 5250 3850 3    60   Input ~ 0
D4
Text GLabel 5350 3850 3    60   Input ~ 0
D5
Text GLabel 5450 3850 3    60   Input ~ 0
D6
Text GLabel 5550 3850 3    60   Input ~ 0
D7
Text GLabel 6450 2800 0    60   Input ~ 0
5V
Text GLabel 6450 2900 0    60   Input ~ 0
GND
Text GLabel 6450 3000 0    60   Input ~ 0
POT
Text GLabel 6450 3100 0    60   Input ~ 0
RS
Text GLabel 6450 3200 0    60   Input ~ 0
E
Text GLabel 6450 3300 0    60   Input ~ 0
D4
Text GLabel 6450 3400 0    60   Input ~ 0
D5
Text GLabel 6450 3500 0    60   Input ~ 0
D6
Text GLabel 6450 3600 0    60   Input ~ 0
D7
Text GLabel 6000 2100 1    60   Input ~ 0
5V
Text GLabel 6400 2100 1    60   Input ~ 0
GND
$Comp
L PWR_FLAG #FLG01
U 1 1 5B23F942
P 6000 2300
F 0 "#FLG01" H 6000 2375 50  0001 C CNN
F 1 "PWR_FLAG" H 6000 2450 50  0000 C CNN
F 2 "" H 6000 2300 50  0001 C CNN
F 3 "" H 6000 2300 50  0001 C CNN
	1    6000 2300
	-1   0    0    1   
$EndComp
$Comp
L PWR_FLAG #FLG02
U 1 1 5B23F960
P 6400 2300
F 0 "#FLG02" H 6400 2375 50  0001 C CNN
F 1 "PWR_FLAG" H 6400 2450 50  0000 C CNN
F 2 "" H 6400 2300 50  0001 C CNN
F 3 "" H 6400 2300 50  0001 C CNN
	1    6400 2300
	-1   0    0    1   
$EndComp
Wire Wire Line
	6400 2100 6400 2300
Wire Wire Line
	6000 2100 6000 2300
Wire Wire Line
	5350 2400 5350 2900
Text GLabel 4950 3850 3    60   Input ~ 0
GND
$EndSCHEMATC
