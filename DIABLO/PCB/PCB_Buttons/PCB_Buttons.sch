EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:switches
LIBS:relays
LIBS:motors
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:Buttons-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L R R1
U 1 1 5B23DABD
P 4750 2850
F 0 "R1" V 4830 2850 50  0000 C CNN
F 1 "10k" V 4750 2850 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 4680 2850 50  0001 C CNN
F 3 "" H 4750 2850 50  0001 C CNN
	1    4750 2850
	0    -1   -1   0   
$EndComp
$Comp
L R R2
U 1 1 5B23DAFC
P 4750 3300
F 0 "R2" V 4830 3300 50  0000 C CNN
F 1 "10k" V 4750 3300 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 4680 3300 50  0001 C CNN
F 3 "" H 4750 3300 50  0001 C CNN
	1    4750 3300
	0    -1   -1   0   
$EndComp
$Comp
L R R3
U 1 1 5B23DB16
P 4750 3750
F 0 "R3" V 4830 3750 50  0000 C CNN
F 1 "10k" V 4750 3750 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 4680 3750 50  0001 C CNN
F 3 "" H 4750 3750 50  0001 C CNN
	1    4750 3750
	0    -1   -1   0   
$EndComp
$Comp
L SW_DIP_x01 SW1
U 1 1 5B23DB30
P 4050 2850
F 0 "SW1" H 4050 3000 50  0000 C CNN
F 1 "SW_DIP_x01" H 4050 2700 50  0000 C CNN
F 2 "switch:SPST-2-PIN_6mm_thru" H 4050 2850 50  0001 C CNN
F 3 "" H 4050 2850 50  0001 C CNN
	1    4050 2850
	1    0    0    -1  
$EndComp
$Comp
L SW_DIP_x01 SW2
U 1 1 5B23DB73
P 4050 3300
F 0 "SW2" H 4050 3450 50  0000 C CNN
F 1 "SW_DIP_x01" H 4050 3150 50  0000 C CNN
F 2 "switch:SPST-2-PIN_6mm_thru" H 4050 3300 50  0001 C CNN
F 3 "" H 4050 3300 50  0001 C CNN
	1    4050 3300
	1    0    0    -1  
$EndComp
$Comp
L SW_DIP_x01 SW3
U 1 1 5B23DB9A
P 4050 3750
F 0 "SW3" H 4050 3900 50  0000 C CNN
F 1 "SW_DIP_x01" H 4050 3600 50  0000 C CNN
F 2 "switch:SPST-2-PIN_6mm_thru" H 4050 3750 50  0001 C CNN
F 3 "" H 4050 3750 50  0001 C CNN
	1    4050 3750
	1    0    0    -1  
$EndComp
$Comp
L POT RV1
U 1 1 5B23DBBA
P 2950 3100
F 0 "RV1" V 2775 3100 50  0000 C CNN
F 1 "POT" V 2850 3100 50  0000 C CNN
F 2 "Potentiometers:Potentiometer_Trimmer_Suntan_TSR-3386P_Horizontal" H 2950 3100 50  0001 C CNN
F 3 "" H 2950 3100 50  0001 C CNN
	1    2950 3100
	0    -1   -1   0   
$EndComp
$Comp
L PWR_FLAG #FLG01
U 1 1 5B23DC05
P 3950 1700
F 0 "#FLG01" H 3950 1775 50  0001 C CNN
F 1 "PWR_FLAG" H 3950 1850 50  0000 C CNN
F 2 "" H 3950 1700 50  0001 C CNN
F 3 "" H 3950 1700 50  0001 C CNN
	1    3950 1700
	1    0    0    -1  
$EndComp
$Comp
L PWR_FLAG #FLG02
U 1 1 5B23DC2C
P 3500 1700
F 0 "#FLG02" H 3500 1775 50  0001 C CNN
F 1 "PWR_FLAG" H 3500 1850 50  0000 C CNN
F 2 "" H 3500 1700 50  0001 C CNN
F 3 "" H 3500 1700 50  0001 C CNN
	1    3500 1700
	1    0    0    -1  
$EndComp
Wire Wire Line
	3950 1700 3950 1900
Text GLabel 3500 1900 3    60   Input ~ 0
5V
Wire Wire Line
	3500 1700 3500 1900
Text GLabel 3950 1900 3    60   Input ~ 0
GND
Wire Wire Line
	4350 2850 4600 2850
Connection ~ 4450 2850
Wire Wire Line
	4350 3300 4600 3300
Connection ~ 4450 3300
Wire Wire Line
	4350 3750 4600 3750
Connection ~ 4450 3750
Text GLabel 5150 3950 3    60   Input ~ 0
GND
Wire Wire Line
	4900 2850 5150 2850
Wire Wire Line
	5150 2850 5150 3950
Wire Wire Line
	4900 3300 5150 3300
Connection ~ 5150 3300
Wire Wire Line
	4900 3750 5150 3750
Connection ~ 5150 3750
Text GLabel 3500 2700 1    60   Input ~ 0
5V
Wire Wire Line
	3500 2700 3500 3750
Wire Wire Line
	3500 3750 3750 3750
Wire Wire Line
	3750 3300 3500 3300
Connection ~ 3500 3300
Wire Wire Line
	3750 2850 3500 2850
Connection ~ 3500 2850
Wire Wire Line
	2950 2850 2950 2950
Text GLabel 3250 3250 3    60   Input ~ 0
GND
Text GLabel 2550 2750 1    60   Input ~ 0
5V
Wire Wire Line
	2550 2750 2550 3100
Wire Wire Line
	2550 3100 2800 3100
Wire Wire Line
	3100 3100 3250 3100
Wire Wire Line
	3250 3100 3250 3250
$Comp
L Conn_01x06 J1
U 1 1 5B23EA6F
P 4900 1600
F 0 "J1" H 4900 1900 50  0000 C CNN
F 1 "Conn_01x06" H 4900 1200 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x06_Pitch2.54mm" H 4900 1600 50  0001 C CNN
F 3 "" H 4900 1600 50  0001 C CNN
	1    4900 1600
	1    0    0    -1  
$EndComp
Text GLabel 4450 1150 1    60   Input ~ 0
5V
Wire Wire Line
	4700 1400 4450 1400
Wire Wire Line
	4450 1400 4450 1150
Wire Wire Line
	4700 1800 4450 1800
Wire Wire Line
	4400 1700 4700 1700
Wire Wire Line
	4350 1600 4700 1600
Text GLabel 4550 2100 3    60   Input ~ 0
GND
Wire Wire Line
	4700 1900 4550 1900
Wire Wire Line
	4550 1900 4550 2100
Text GLabel 2950 2850 1    60   Input ~ 0
POT
Text GLabel 4400 1500 0    60   Input ~ 0
POT
Wire Wire Line
	4700 1500 4400 1500
Wire Wire Line
	4450 1800 4450 2850
Wire Wire Line
	4450 2900 4450 3300
Wire Wire Line
	4400 3350 4450 3350
Wire Wire Line
	4450 3350 4450 3750
Wire Wire Line
	4450 2900 4400 2900
Wire Wire Line
	4400 2900 4400 1700
Wire Wire Line
	4350 1600 4350 2950
Wire Wire Line
	4350 2950 4400 2950
Wire Wire Line
	4400 2950 4400 3350
$EndSCHEMATC
