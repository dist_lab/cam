EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:switches
LIBS:relays
LIBS:motors
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:PeltierFanPCB-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Conn_01x02 J5
U 1 1 5B1FF1D5
P 8250 1200
F 0 "J5" H 8250 1300 50  0000 C CNN
F 1 "Alim" H 8250 1000 50  0000 C CNN
F 2 "Connectors:bornier2" H 8250 1200 50  0001 C CNN
F 3 "" H 8250 1200 50  0001 C CNN
	1    8250 1200
	1    0    0    -1  
$EndComp
$Comp
L LED D1
U 1 1 5B1FF261
P 6400 2000
F 0 "D1" H 6400 2100 50  0000 C CNN
F 1 "LED" H 6400 1900 50  0000 C CNN
F 2 "LEDs:LED_D4.0mm" H 6400 2000 50  0001 C CNN
F 3 "" H 6400 2000 50  0001 C CNN
	1    6400 2000
	0    -1   -1   0   
$EndComp
$Comp
L R R1
U 1 1 5B1FF290
P 5400 2550
F 0 "R1" V 5480 2550 50  0000 C CNN
F 1 "10" V 5400 2550 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 5330 2550 50  0001 C CNN
F 3 "" H 5400 2550 50  0001 C CNN
	1    5400 2550
	0    1    1    0   
$EndComp
$Comp
L Conn_01x01 J1
U 1 1 5B1FF31D
P 4850 2550
F 0 "J1" H 4850 2650 50  0000 C CNN
F 1 "Pin Arduino1" H 4850 2450 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x01_Pitch1.00mm" H 4850 2550 50  0001 C CNN
F 3 "" H 4850 2550 50  0001 C CNN
	1    4850 2550
	-1   0    0    1   
$EndComp
$Comp
L R R2
U 1 1 5B1FF367
P 5400 2800
F 0 "R2" V 5480 2800 50  0000 C CNN
F 1 "100k" V 5400 2800 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 5330 2800 50  0001 C CNN
F 3 "" H 5400 2800 50  0001 C CNN
	1    5400 2800
	0    1    1    0   
$EndComp
Text GLabel 7750 1200 0    60   Input ~ 0
12V
Text GLabel 7750 1300 0    60   Input ~ 0
GND
$Comp
L R R5
U 1 1 5B1FF4FB
P 6400 1650
F 0 "R5" V 6480 1650 50  0000 C CNN
F 1 "1.8k" V 6400 1650 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 6330 1650 50  0001 C CNN
F 3 "" H 6400 1650 50  0001 C CNN
	1    6400 1650
	-1   0    0    1   
$EndComp
Wire Wire Line
	5550 2550 5700 2550
Wire Wire Line
	5250 2550 5050 2550
Wire Wire Line
	5150 2550 5150 2800
Wire Wire Line
	5150 2800 5250 2800
Connection ~ 5150 2550
Wire Wire Line
	5550 2800 6000 2800
Wire Wire Line
	6000 2750 6000 2950
Wire Wire Line
	8050 1200 7750 1200
Wire Wire Line
	8050 1300 7750 1300
Wire Wire Line
	6400 1800 6400 1850
Wire Wire Line
	6400 2150 6400 2250
Wire Wire Line
	6000 2250 7050 2250
Wire Wire Line
	6000 2250 6000 2350
Wire Wire Line
	6400 1500 6400 1400
Text GLabel 6400 1400 1    60   Input ~ 0
12V
$Comp
L Conn_01x02 J3
U 1 1 5B1FF632
P 7250 2150
F 0 "J3" H 7250 2250 50  0000 C CNN
F 1 "Sortie1" H 7250 1950 50  0000 C CNN
F 2 "Connectors:bornier2" H 7250 2150 50  0001 C CNN
F 3 "" H 7250 2150 50  0001 C CNN
	1    7250 2150
	1    0    0    -1  
$EndComp
Connection ~ 6400 2250
Wire Wire Line
	7050 2150 6600 2150
Wire Wire Line
	6600 2150 6600 1450
Wire Wire Line
	6600 1450 6400 1450
Connection ~ 6400 1450
Connection ~ 6000 2800
Text GLabel 6000 2950 3    60   Input ~ 0
GND
$Comp
L LED D2
U 1 1 5B1FF8DA
P 6400 3800
F 0 "D2" H 6400 3900 50  0000 C CNN
F 1 "LED" H 6400 3700 50  0000 C CNN
F 2 "LEDs:LED_D4.0mm" H 6400 3800 50  0001 C CNN
F 3 "" H 6400 3800 50  0001 C CNN
	1    6400 3800
	0    -1   -1   0   
$EndComp
$Comp
L R R3
U 1 1 5B1FF8E0
P 5400 4350
F 0 "R3" V 5480 4350 50  0000 C CNN
F 1 "10" V 5400 4350 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 5330 4350 50  0001 C CNN
F 3 "" H 5400 4350 50  0001 C CNN
	1    5400 4350
	0    1    1    0   
$EndComp
$Comp
L Conn_01x01 J2
U 1 1 5B1FF8E6
P 4850 4350
F 0 "J2" H 4850 4450 50  0000 C CNN
F 1 "Pin Arduino2" H 4850 4250 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x01_Pitch1.00mm" H 4850 4350 50  0001 C CNN
F 3 "" H 4850 4350 50  0001 C CNN
	1    4850 4350
	-1   0    0    1   
$EndComp
$Comp
L R R4
U 1 1 5B1FF8EC
P 5400 4600
F 0 "R4" V 5480 4600 50  0000 C CNN
F 1 "100k" V 5400 4600 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 5330 4600 50  0001 C CNN
F 3 "" H 5400 4600 50  0001 C CNN
	1    5400 4600
	0    1    1    0   
$EndComp
$Comp
L R R6
U 1 1 5B1FF8F2
P 6400 3450
F 0 "R6" V 6480 3450 50  0000 C CNN
F 1 "1.8k" V 6400 3450 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 6330 3450 50  0001 C CNN
F 3 "" H 6400 3450 50  0001 C CNN
	1    6400 3450
	-1   0    0    1   
$EndComp
Wire Wire Line
	5550 4350 5700 4350
Wire Wire Line
	5250 4350 5050 4350
Wire Wire Line
	5150 4350 5150 4600
Wire Wire Line
	5150 4600 5250 4600
Connection ~ 5150 4350
Wire Wire Line
	5550 4600 6000 4600
Wire Wire Line
	6000 4550 6000 4750
Wire Wire Line
	6400 3600 6400 3650
Wire Wire Line
	6400 3950 6400 4050
Wire Wire Line
	6000 4050 7050 4050
Wire Wire Line
	6000 4050 6000 4150
Wire Wire Line
	6400 3300 6400 3200
Text GLabel 6400 3200 1    60   Input ~ 0
12V
$Comp
L Conn_01x02 J4
U 1 1 5B1FF905
P 7250 3950
F 0 "J4" H 7250 4050 50  0000 C CNN
F 1 "Sortie2" H 7250 3750 50  0000 C CNN
F 2 "Connectors:bornier2" H 7250 3950 50  0001 C CNN
F 3 "" H 7250 3950 50  0001 C CNN
	1    7250 3950
	1    0    0    -1  
$EndComp
Connection ~ 6400 4050
Wire Wire Line
	7050 3950 6600 3950
Wire Wire Line
	6600 3950 6600 3250
Wire Wire Line
	6600 3250 6400 3250
Connection ~ 6400 3250
Connection ~ 6000 4600
Text GLabel 6000 4750 3    60   Input ~ 0
GND
$Comp
L PWR_FLAG #FLG01
U 1 1 5B20020C
P 8450 2300
F 0 "#FLG01" H 8450 2375 50  0001 C CNN
F 1 "PWR_FLAG" H 8450 2450 50  0000 C CNN
F 2 "" H 8450 2300 50  0001 C CNN
F 3 "" H 8450 2300 50  0001 C CNN
	1    8450 2300
	-1   0    0    1   
$EndComp
$Comp
L PWR_FLAG #FLG02
U 1 1 5B20024B
P 8050 2300
F 0 "#FLG02" H 8050 2375 50  0001 C CNN
F 1 "PWR_FLAG" H 8050 2450 50  0000 C CNN
F 2 "" H 8050 2300 50  0001 C CNN
F 3 "" H 8050 2300 50  0001 C CNN
	1    8050 2300
	-1   0    0    1   
$EndComp
Text GLabel 8050 2000 1    60   Input ~ 0
12V
Text GLabel 8450 2000 1    60   Input ~ 0
GND
Wire Wire Line
	8050 2300 8050 2000
Wire Wire Line
	8450 2000 8450 2300
$Comp
L Q_NMOS_GDS Q1
U 1 1 5B22BF29
P 5900 2550
F 0 "Q1" H 6100 2600 50  0000 L CNN
F 1 "Q_NMOS_GDS" H 6100 2500 50  0000 L CNN
F 2 "TO_SOT_Packages_THT:TO-220_Vertical" H 6100 2650 50  0001 C CNN
F 3 "" H 5900 2550 50  0001 C CNN
	1    5900 2550
	1    0    0    -1  
$EndComp
$Comp
L Q_NMOS_GDS Q2
U 1 1 5B22BF74
P 5900 4350
F 0 "Q2" H 6100 4400 50  0000 L CNN
F 1 "Q_NMOS_GDS" H 6100 4300 50  0000 L CNN
F 2 "TO_SOT_Packages_THT:TO-220_Vertical" H 6100 4450 50  0001 C CNN
F 3 "" H 5900 4350 50  0001 C CNN
	1    5900 4350
	1    0    0    -1  
$EndComp
$EndSCHEMATC
