/*********************************************************************************
 ******************** Sylvain KRIEGER 2018 DIABLO ********************************
 ******************** Prototype for the Peltier and the Arduino Nano only ********
 ******************** Last update : 19/06/2018 ***********************************
**********************************************************************************/

//Libraries
#include <PID_v1.h>             //PID's library
#include <LiquidCrystal.h>      //LCD's Library
#include <thermistor.h>         //Thermistor's Library 

//LCD
#define RSPIN 2
#define ENPIN 3
#define D4PIN 4
#define D5PIN 5
#define D6PIN 6 
#define D7PIN 7

#define ROWS 2
#define COLUMNS 16

//Pins
#define HOTPIN 10               //Peltier's pin D10
#define THERMIPIN A0            //Thermistor's pin T0
#define FANPIN 11               //Fan's pin D9
#define UPPIN 8                 //Button's pin D35
#define DOWNPIN 9               //Button's pin D31
#define ENTERPIN 12             //Button's pin D33

//Constants
#define ERRORTEMP 0         //Error between the thermometer and the sensor
#define INERTIA 0.5             //Inertie of the temperature
#define NBSAMPLES 10            //Number of samples for the sensor

#define MAXPWM 255              //Maximum value for the PWM
#define MINPWM 0                //Minimum value for the PWM

#define MAXTIME 120             //Maximum time
#define MAXTEMP 150             //Maximum temperature
#define MINTIME 5               //Minimum time
#define MINTEMP 30              //Minimum temperature

#define TEMPLAMP 60             //Default temperature for the LAMP
#define TIMELAMP 60             //Default time for the LAMP
#define BASICPOSITION 0         //Default position for the cursor on the LCD

//PID
#define KP 200.0    //200
#define KI 10.0    //4.0
#define KD 12.0     //12.0

/****************************************************************************************************
*****************************************************************************************************
****************************************************************************************************/

//Variables for the time, the position of the cursor and for the temperature
unsigned char samples, enter, pos, posCursor, Start;
short myTime, myTemp, second, minute;
double sensorTemp = 0.0;

/****************************************************************************************************
*****************************************************************************************************
****************************************************************************************************/

//Prototypes
void automate(void);
void displayLCD1  (LiquidCrystal myLCD, double myTemperature);
void displayLCD2  (LiquidCrystal myLCD, double myTemperature, short myMinute, short mySecond);
void displayLCD3  (LiquidCrystal myLCD);
void displayCursor(LiquidCrystal myLCD, unsigned char myCursor);
void actualizeTime(float timeBase);
void makeCurve (double myTemperature);
double readTemp(thermistor myThermistor, long numberSamples, double myTemperature);

/****************************************************************************************************
*****************************************************************************************************
****************************************************************************************************/

//Initialisations
//LCD's initialisation
LiquidCrystal lcd(RSPIN, ENPIN, D4PIN, D5PIN, D6PIN, D7PIN);

//Parameters and initialisation of the PID
double Setpoint, Input, Output;
PID myPID(&Input, &Output, &Setpoint, KP, KI, KD, DIRECT);

//Initialisation of the thermistor, for more informations, watch the configuration.h
thermistor therm1(THERMIPIN,0);  

/****************************************************************************************************
*****************************************************************************************************
****************************************************************************************************/

void setup() {
  pinMode(HOTPIN, OUTPUT);      //The Peltier's pin is an output
  pinMode(FANPIN, OUTPUT);      //The Fan's pin is an output
  pinMode(UPPIN, INPUT);        //The button's pin is an input
  pinMode(DOWNPIN, INPUT);      //The button's pin is an input  
  pinMode(ENTERPIN, INPUT);     //The button's pin is an input 

  digitalWrite(FANPIN, HIGH);   //Set the value of the fan

  myPID.SetMode(AUTOMATIC);     //Start the PID process
  
  second=0; minute=0;           //Reset the time

  myTemp=TEMPLAMP;              //Set the temperature at the default value
  myTime=TIMELAMP;              //Set the time at the default value

  Serial.begin(9600);           //Start the serial communication between your computer and the arduino
  Serial.println("DIABLO NANO");

  //Initialize the LCD
  lcd.begin(COLUMNS, ROWS);
  lcd.setCursor(1,0);
  lcd.print("PROTO DIABLO");
  
  delay(2000);
}

/****************************************************************************************************
*****************************************************************************************************
****************************************************************************************************/

void loop() {
  //Display the menu on the LCD
  posCursor=pos%ROWS;
  automate();
  displayCursor(lcd, posCursor);
  
  //Start the heating if the flag is on
  if(Start==1) {
    
    //Read the samples
    sensorTemp = readTemp(therm1, NBSAMPLES, sensorTemp);
    
    //While Temperature<<Setpoint, write the maximum value on the Peltier
    while ((sensorTemp<myTemp-INERTIA)&&digitalRead(UPPIN)==LOW) {
        analogWrite(HOTPIN, MAXPWM);    //Write the maximum value for the Peltier
  
        sensorTemp = readTemp(therm1, NBSAMPLES, sensorTemp);
        
        //Display the temperature and the value of the PWM on a LCD
        displayLCD1(lcd, sensorTemp);
        
        //Make a curve of the temperature on the Serial plotter
        makeCurve(sensorTemp);
        
        delay(100);
    }
  
    //Start the control when the temperature is near the Setpoint
    while((minute < myTime)&&digitalRead(UPPIN)==LOW) {
            sensorTemp = readTemp(therm1, NBSAMPLES, sensorTemp);
            
            Input = sensorTemp;                       //For the PID
            myPID.Compute();                          //Compute the value for the Peltier       
            analogWrite(HOTPIN, Output);              //Write the value from the PID to the Peltier's pin  
            
            //Display the temperature and the value of the PWM on the LCD
            displayLCD2(lcd, sensorTemp, minute, second);
  
            //Make a curve of the temperature on the Serial plotter
            makeCurve(sensorTemp);
            
            //Actualize the time
            actualizeTime(0);
          }
  
    //Stop the process after the time that you have decided
    if(minute>=myTime) {
    analogWrite(HOTPIN, MINPWM);            //Stop the Peltier
    displayLCD3(lcd); }
    }
  delay(100);
}

/****************************************************************************************************
*****************************************************************************************************
****************************************************************************************************/

void automate(void) {
  typedef enum {State_Principale=0, State_LAMP=1, State_Start_LAMP=2, State_Manual_1=3, State_Manual_2=4, State_Temperature=5,
                State_Time=6, State_Start_Manual=7, State_Select_Temperature=8, State_Select_Time=9} type_state;
  type_state state_actuel;
      static type_state state_futur=State_Principale;
      
    // gestion des entrees
    // -------------------
    if (digitalRead(UPPIN)==HIGH && digitalRead(DOWNPIN)==LOW && digitalRead(ENTERPIN)==LOW){
      delay(200);
      if (digitalRead(UPPIN)==LOW && digitalRead(DOWNPIN)==LOW && digitalRead(ENTERPIN)==LOW) pos=pos-1; }
      
    if (digitalRead(UPPIN)==LOW && digitalRead(DOWNPIN)==HIGH && digitalRead(ENTERPIN)==LOW){
      delay(200); 
      if (digitalRead(UPPIN)==LOW && digitalRead(DOWNPIN)==LOW && digitalRead(ENTERPIN)==LOW) pos=pos+1; }

    if(digitalRead(ENTERPIN)==HIGH){
      delay(200);
      if(digitalRead(ENTERPIN)==LOW) enter = HIGH; }
    else enter = LOW;
    
    // gestion du diagramme de transition
    // ----------------------------------
    state_actuel = state_futur;
    switch(state_actuel) {
        case State_Principale :
            pos=pos%2;
            
            if (pos==BASICPOSITION&&enter==HIGH){ 
              state_futur = State_LAMP;
              pos = BASICPOSITION;
            }
            else if (pos>=1&&enter==HIGH){
              state_futur = State_Manual_1;
              pos = BASICPOSITION;
            }
            break;
            
        case State_LAMP :
            pos=pos%2;
            
            if (pos==BASICPOSITION&&enter==HIGH){
              state_futur = State_Start_LAMP;
              pos = BASICPOSITION;
            }
            else if (pos>=1&&enter==HIGH){
              state_futur = State_Principale;
              pos = BASICPOSITION;
            }
            break;
            
        case State_Start_LAMP :
            state_futur = State_Principale;
            pos = BASICPOSITION;
            break;

        case State_Manual_1 :
            pos=pos%(2*ROWS);
            if (pos==BASICPOSITION&&enter==HIGH){
              state_futur = State_Start_Manual;
              pos = BASICPOSITION;
            }
            else if (pos==1&&enter==HIGH){
              state_futur = State_Temperature;
              pos = BASICPOSITION;
            }
            else if(pos>1) state_futur = State_Manual_2;
            break;
            
        case State_Manual_2 :
            pos=pos%(2*ROWS);
            if (pos==2&&enter==HIGH){
              state_futur = State_Time;
              pos = BASICPOSITION;
            }
            else if (pos==3&&enter==HIGH){
              state_futur = State_Principale;
              pos = BASICPOSITION;
            }
            else if(pos<2) state_futur = State_Manual_1;
            break;
            
        case State_Temperature :
            pos=pos%2;
            
            if (pos==BASICPOSITION&&enter==HIGH){    
              state_futur = State_Select_Temperature;
              pos = BASICPOSITION;
            }
            if (pos>=1&&enter==HIGH){
              state_futur = State_Manual_1;
              pos = BASICPOSITION;
            }
            break;

        case State_Time :
            pos=pos%2;
            if (pos==BASICPOSITION&&enter==HIGH){
              state_futur = State_Select_Time;
              pos = BASICPOSITION;
            }
            if (pos>=1&&enter==HIGH){
              state_futur = State_Manual_2;
              pos = BASICPOSITION;
            }
            break;
            
        case State_Start_Manual :
            state_futur = State_Principale;
            pos = BASICPOSITION;
            break;
            
        case State_Select_Temperature :
            pos = BASICPOSITION;
            if(enter==HIGH){
              pos = BASICPOSITION;
              state_futur = State_Temperature;
            }
            else {
              if (digitalRead(UPPIN)==LOW && digitalRead(DOWNPIN)==HIGH && digitalRead(ENTERPIN)==LOW)myTemp=myTemp-5;
              if (digitalRead(UPPIN)==HIGH && digitalRead(DOWNPIN)==LOW && digitalRead(ENTERPIN)==LOW)myTemp=myTemp+5;
              if(myTemp<=MINTEMP)  myTemp=MINTEMP;
              if(myTemp>=MAXTEMP) myTemp=MAXTEMP;
            }
            break;
            
        case State_Select_Time :
            pos = BASICPOSITION;
            if(enter==HIGH){ 
              pos = BASICPOSITION;
              state_futur = State_Time;
            }
            else {
              if (digitalRead(UPPIN)==LOW && digitalRead(DOWNPIN)==HIGH && digitalRead(ENTERPIN)==LOW)myTime=myTime-5;
              if (digitalRead(UPPIN)==HIGH && digitalRead(DOWNPIN)==LOW && digitalRead(ENTERPIN)==LOW)myTime=myTime+5;
              if(myTime<=MINTIME) myTime=MINTIME;
              if(myTime>=MAXTIME)myTime=MAXTIME;
            }
            break;
    }
    // gestion des sorties
    // -------------------
    switch(state_actuel) {
        case State_Principale :
            Start=0;
            lcd.clear();
            lcd.setCursor(0,0);
            lcd.print(" LAMP");
            lcd.setCursor(0,1);
            lcd.print(" MANUAL");
            break;
            
        case State_LAMP :
            Start=0;
            lcd.clear();
            lcd.setCursor(0,0);
            lcd.print(" INICIAR");
            lcd.setCursor(0,1);
            lcd.print(" BACK");
            break;
            
        case State_Start_LAMP :
            myTemp=TEMPLAMP;
            myTime=TIMELAMP;
            Setpoint=TEMPLAMP;
            Start=1;
            delay(500);
            break;
            
        case State_Manual_1 :
            Start=0;
            lcd.clear();
            lcd.setCursor(0,0);
            lcd.print(" INICIAR");
            lcd.setCursor(0,1);
            lcd.print(" TEMP *C");
            break;
            
        case State_Manual_2 :
            Start=0;
            lcd.clear();
            lcd.setCursor(0,0);
            lcd.print(" TIME");
            lcd.setCursor(0,1);
            lcd.print(" BACK");
            break;
            
        case State_Temperature :
            Start=0;
            lcd.clear();
            lcd.setCursor(0,0);
            lcd.print(" TEMP : ");
            lcd.print(myTemp);
            lcd.setCursor(0,1);
            lcd.print(" BACK");
            break;
           
        case State_Select_Temperature :
            Start=0;
            lcd.clear();
            lcd.setCursor(0,0);
            lcd.print(" TEMP : ");
            lcd.print(myTemp);
            break;
            
        case State_Time :
            Start=0;
            lcd.clear();
            lcd.setCursor(0,0);
            lcd.print(" TIME : ");
            lcd.print(myTime);
            lcd.setCursor(0,1);
            lcd.print(" BACK");
            break;

        case State_Select_Time :
            Start=0;
            lcd.clear();
            lcd.setCursor(0,0);
            lcd.print(" TIME : ");
            lcd.print(myTime);
            break;
            
        case State_Start_Manual :
            Setpoint=myTemp;
            Start=1;
            delay(500);
            break;
    }
}

/****************************************************************************************************
*****************************************************************************************************
****************************************************************************************************/

void displayLCD1(LiquidCrystal myLCD, double myTemperature) {
  myLCD.clear();
  myLCD.setCursor(0,0);
  myLCD.print("Temp : ");
  myLCD.print(myTemperature);
  myLCD.print(" *C");
  myLCD.setCursor(0,1);
  myLCD.print("Heating...");
}

/****************************************************************************************************
*****************************************************************************************************
****************************************************************************************************/

void displayLCD2(LiquidCrystal myLCD, double myTemperature, short myMinute, short mySecond) {
  myLCD.clear();
  myLCD.print("Temp : ");
  myLCD.print(myTemperature);
  myLCD.print(" *C");
  myLCD.setCursor(0, 1);
  myLCD.print("Time :  ");
  myLCD.print(myMinute); 
  myLCD.print(":");
  myLCD.print(mySecond);
}

/****************************************************************************************************
*****************************************************************************************************
****************************************************************************************************/

void displayLCD3(LiquidCrystal myLCD){
  Serial.println("End of the reaction");
  myLCD.clear();
  myLCD.setCursor(3,0);
  myLCD.print("End of the");
  myLCD.setCursor(3,1);
  myLCD.print("process");
  delay(20000);
}

/****************************************************************************************************
*****************************************************************************************************
****************************************************************************************************/

void displayCursor(LiquidCrystal myLCD, unsigned char myCursor) {
  myLCD.setCursor(BASICPOSITION, myCursor);
  myLCD.print(">");
}

/****************************************************************************************************
*****************************************************************************************************
****************************************************************************************************/

void actualizeTime(float timeBase) {
  if (timeBase==0) {
   second = second+1;
    if (second==60) {
      second = 0;
      minute = minute+1;
    }
  }
  else if (timeBase!=1) {
    second = second+(timeBase/1000);
    if (second==60) {
      second = 0;
      minute = minute+1;
    }
    delay(timeBase);
  }
}

/****************************************************************************************************
*****************************************************************************************************
****************************************************************************************************/

void makeCurve(double myTemperature){
  Serial.print("*C");
  Serial.print(",");
  Serial.println(myTemperature);
}

/****************************************************************************************************
*****************************************************************************************************
****************************************************************************************************/

double readTemp(thermistor myThermistor, long numberSamples, double myTemperature) {
  long samples;
  for(samples=0; samples<numberSamples; samples++) {
     myTemperature = (myThermistor.analog2temp()-ERRORTEMP)+myTemperature;
     delay(100); }
  myTemperature = myTemperature/numberSamples;
  return myTemperature;
}

